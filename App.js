import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Sharing from "expo-sharing";

export default function App() {
  const [selectedImage, setSelectedImage] = useState(null);

  const openShareDialog = async () => {
    if (!(await Sharing.isAvailableAsync())) {
      alert("La imagen no se puede compartir");
      return;
    }
    await Sharing.shareAsync(selectedImage.localUri);
  };

  let openImagePickerAsync = async () => {
    let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("El permiso es requerido");
      return;
    }
    const pickerResult = await ImagePicker.launchImageLibraryAsync();
    if (pickerResult.cancelled === true) {
      return;
    }
    setSelectedImage({ localUri: pickerResult.uri });
  };
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Bienvenidos</Text>

      <TouchableOpacity onPress={openImagePickerAsync}>
        <Image
          style={styles.image}
          source={{
            uri:
              selectedImage !== null
                ? selectedImage.localUri
                : "https://c.files.bbci.co.uk/12A9B/production/_111434467_gettyimages-1143489763.jpg",
          }}
        />
      </TouchableOpacity>

      {selectedImage ? (
        <TouchableOpacity onPress={openShareDialog} style={styles.button}>
          <Text>Compartir Imagen</Text>
        </TouchableOpacity>
      ) : (
        <View />
      )}

      <TouchableOpacity
        style={styles.button}
        onPress={() =>
          Alert.alert("¡Hola!", "¿Te gustan los gatos?", [
            {
              text: "Si",
              onPress: () => console.log("Si"),
            },
            { text: "No", onPress: () => console.log("No") },
          ])
        }
      >
        <Text>Pregunta</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1e6f5c",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "white",
    fontSize: 50,
    marginTop: 30,
  },
  image: {
    height: 400,
    width: 400,
    margin: 10,
    resizeMode: "contain",
  },
  button: {
    backgroundColor: "white",
    padding: 10,
    margin: 10,
  },
});
